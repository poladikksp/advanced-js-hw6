// Асинхронність в JS - коли код що потребує деякого часу для виконання не зупиняє виконання всього коду, а виконується в фоні при цьому программа продовжує виконувати інший код.

async function getIp() {
  const response = await fetch("https://api.ipify.org/?format=json");
  const { ip } = await response.json();
  return ip;
}
async function getGeo(ip) {
  const response = await fetch("http://ip-api.com/json/" + ip);
  return await response.json();
}
async function renderInfo(parentNode) {
  const ip = await getIp();
  const adress = await getGeo(ip);

  const { timezone: continent, country, region, city, lat, lon } = adress;
  const adressList = `
  <ul>
  <li>Continent: ${continent.split("/")[0]}</li>
  <li>Country: ${country}</li>
  <li>Region: ${region}</li>
  <li>City: ${city}</li>
  <li>Latitude: ${lat}°</li>
  <li>Longitude: ${lon}°</li>
  </ul>
  `;
  parentNode.insertAdjacentHTML("beforeend", adressList);
}

const findButton = document.querySelector("#find");
findButton.addEventListener("click", () => {
  renderInfo(document.querySelector(".info"));
});
